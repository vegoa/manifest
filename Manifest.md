The Vegoa Project
=================

* Why? Vegoa wants to lead by example promoting Veganism and Sustainability
* How? Vegoans will live in cohousing projects, farm community gardens, create Vegan cooperatives, promote vegan events and local activities
* What? Vegoa is a vegan transition movement, vegan community and platform for vegan initiatives.
* Where? South of Portugal
Social Organisation - Shared Resources (Commons), Transparency, Decentralisation, Equality
Values - Veganism, Sustainability, Freedom

FAQ
===

How do I join?
--------------
* You may want to visit Vegoa before you decide to join. Once you decide to join you will be asked for a one time donation of 500€ that will help Vegoa welcome you and continue promoting its mission.

What will the 500€ be used for?
-------------------------------
* Cooperative expenses (statute cost, …)
* Cooperative insurance (to cover any legal actions against outside world)
* Marketing to promote Vegoa to the outside world
* Create a better hosting structure for the newcomers (ie. HQ office, …)
* Wage for the professional accountant

Is there any other fees?
------------------------
* No, there is no tax, fees or rents within the community. Of course, taxes owed to the Portuguese government need to be paid regularly.

Will I receive a house?
-----------------------
* There will be different cohousing projects that you can join; existing ones or projects needing your financial, physical and intellectual help. Once the project has taken off you can rent accommodation at Vegoa or share a house with members of the cohousing project while building it.

How do I sustain myself and family when in Vegoa?
-------------------------------------------------
* We believe financial security is a human right, for that reason we actively support collective entrepreneurship and offer an unconditional basic income to every Vegoan.

What if the project fails?
--------------------------
* Financial sustainability and social organisation are very important for Vegoa resulting in a safe community environment. But in case unforeseen situation forces the project to finish your house, the plot of land for your house and your belongings are owned by you under the Portuguese law.

What happens to the land? Will it be split? Will the plot where my house is on top belong to me?
----------------------------------------------------
* Yes, the plot (house space) belongs to you.

How fast can I move?
--------------------
* You can move as fast as you want, but please contact us before you do. This will allow us to help you feel welcome and get settled the best way possible.

Are kids welcome?
-----------------
* Kids are very welcome and an important part of Vegoa future.

Can I bring my paw friends?
---------------------------
* Of course :)

Is there a trial period before I become a Vegoan?
-------------------------------------------------
* During the first 6 months after you joined, Vegoa members can oppose to your joining. If a third of them do so you will be asked to leave the community. After this trial period you officially become a Vegoan. You will, however, already have the right to basic income during this trial period.

Can someone be expelled?
------------------------
* Vegoans can only be expelled from the community if they do not respect the Vegoa manifest and even then only with the approval of 90% of Vegoans. This does not mean that they lose their property.

Why should I join?
------------------
* Everyone can join for different reasons, Vegoa is a community and platform for every vegan to thrive.

What commitment is expected from me in the community? (any minimal hours to commit?)
-----------------------------------------------------
* There is no minimal hours or expected commitment. You will be required, however, to accept the manifest.
* Every member can choose whether or not to join any organization and work on it. Every member can also create their own organization.
* Every member is free to volunteer or help the community with its organization.
* Individual creativity is being encouraged by allowing each Vegoan to live with a basic income without imposing any unnecessary rules.

Economy
=======

Vegoa aims to have a self-sustainable economy that prioritises equality between all Vegoans. We achieve it by introducing a basic income and our own currency that incentivises to buy and sell inside the community. Simultaneously when buying a product or service Vegoans are voting in community initiatives. This way we create a vegan sustainable economy where we can guarantee that all products are created by the community and the values it shares.

Basic Income
------------
Vegetas are co-created by all Vegoans.
Every Vegoan receives a monthly basic income in the Vegetas currency.
Every year the basic income grows 10% to guarantee equality (or ~0.73% monthly).
The basic income results in an organic voting system where vegetas are naturally distributed to the organisations whose work is the most required/appreciated.

Vegetas
-------
Vegetas can be used to buy products or services of a Vegoa organisation.
Every Vegoan can receive more Vegetas participating in an Organisation that sells services/products in vegetas.
Vegetas can also be used to create a new Organisation if needed.

Trading
-------
Organisations can trade in Vegetas within the community and with euros/other currencies with the outside world.
Organisations that trade products created outside Vegoa can charge euros for the product itself and Vegetas for the service of selling those products.
Products made inside Vegoa should be traded using Vegetas.
Services inside Vegoa are always priced in Vegetas for Vegoans but the organisations can offer their services in euros as well.

FAQ
===
Can I buy Euros with my Vegetas?
--------------------------------
* Vegetas can be traded with other currencies informally. No exchange system is available.

Can non-Vegoans have/buy Vegetas?
---------------------------------
* No, Vegetas are only co-created from vegoans and belong to vegoans.

Do kids get a basic income?
---------------------------
* Yes, kids get a basic income so we don't create an age inequality.

Does everyone get the same basic income?
----------------------------------------
* Every Vegoan get the same basic income amount.

Are all products inside Vegoa sold in Vegetas?
----------------------------------------------
* It is up to the Organisations to decide if it is financially sustainable to buy in euros and sell in Vegetas but all services and products created inside Vegoa should be sold in Vegetas.

How do you define if a product is created in Vegoa or not?
--------------------------------------------------
* We do not need to define it because there are several benefits for organisations to sell their products in vegetas.

If I have my own business does it have to become part of Vegoa?
------------------------------------------------
* No, Vegoa is just a recommendation. Your own business will stay yours.

Why will the basic income get a yearly 10% growth?
--------------------------------------------------
* Thanks to the 10% growth every year, the relative amount every member owns will be balanced.
The 10% growth is based on the average lifespan of human-beings.
* As an example nowadays we use taxes to redistribute money from the lucky people to the less lucky ones that frequently find solutions to avoid them.

My organisation receives euros and vegetas, can I receive both as an income?
-------------------------------------------------
* As long as your organisation receives euros, as a member, you can suggest and decide all together a certain amount of your income in euros and another amount in vegetas. This makes sense as long as you need euros to get something from outside that we don’t have yet in the community.

Why should I use vegetas if I have the choice to use any other currencies?
----------------------------------------------------
* By using vegetas you will contribute to a vegan economy and therefore support vegan people and their values. It’s like a better version of the vegan stamp that ensure end-to-end cruelty-free exchanges.
* Vegetas is a decentralized digital currency (crypto-currency) not issued or controlled by any governments or financial world.
* Therefore no VAT or any taxes are applied to the transactions or incomes payment between members/organisations.

How can I use my Vegetas?
-------------------------
* Various softwares, such as web and mobile apps will be available to exchange vegetas between members.

Does the Vegoa economy naturally tend to a sustainable ecosystem and influence its members and organisations to be sustainable? Is the Vegoa economy consistent with a sustainable lifestyle?
---------------------------------------------------
* Nowadays, in our society, countries unilaterally or with consensus decide how they will use their global budget with taxes/private money creation and mainly  investing in non-sustainable energies following their own interests.
* In Vegoa, there is no such things as a global budget, every Vegoan gets their basic income and decide at their own scale which investment or not they want to follow. Therefore each Vegoan will naturally prefer to pay less and get more that will end up with sustainable decisions.

Organizations
=============

A Vegoa Organisation is a working group of Vegoans that get together to offer a service, a product or a solution to a problem or need. They are a vital part of Vegoa decentralised structure.

4 types of Organisations
------------------------
1. Open Organisations - Every Vegoan can join an Open Organisation at any time, these Organisations manage common resources or common services like social commons.
2. Co-housing -  Created to manage housing and habitational projects, can handle every aspect of communal living. Some co-housing organisations may have limits based on the availability of houses. Please be aware that you have to either join or create your own cohousing organization in order to have a land where you can live.
3. Services and Products Organisations - Similar to regular cooperatives, these Organisations can be initiated by a group of Vegoans to offer a service or product inside or outside the Vegoa community. Members of these organisations collectively decide when to invite new members to join. Vegoa promotes entrepreneurship to guaranty self sustainability of the Vegoa Project.
4. Land Management Association - These organisations manage land plots and everyone that uses the land decide how to self-manage it.

Basics of an Organisation
-------------------------
Vegoa Organisations are not for profit but guarantee wages to members with the wage values being decided by everyone in that Organisation collectively;
Members of an Organisation collectively decide what management methodology to use: e.g. holacracy, democratic decision making or even regular hierarchy between others forms of organisation.
Organisations are 100% transparent and information about it can be requested by any Vegoan at the end of each quarter. Vegoa incentivises Organisations to share all possible data online.
Knowledge is shared for free between organisations and Vegoans inside the Vegoa Community; organisations are not forced to spend resources required for making the knowledge available, but are encouraged to do so.

FAQ
===

How do organisations decide how much money to pay their members?
-------------------------------------------------
* Every member can suggest how much other members should earn, collectively they decide how much each other can get and when.

As a Services and Products Organisation, can I buy a land ?
----------------------------------------------------
* No, land is managed by a Land association of people that use the land.

Does an Organisation need to be registered legally in Portugal or other country?
--------------------------------------------------
* No, Vegoa organisations are just informal group organisations. However, when trading with the outside world and/or when the Organisation is of big size, registering a legal entity in Portugal (such as a cooperative) should be considered.

How are the organisations managed?
----------------------------------
* Their members decide, they can use any management technique at their own choice (this include money management as well).
* Holacracy is the recommended form of organisation, with this methodology all the members have an equal say.

Is there a retirement scheme available?
---------------------------------------
* It is up to each Organisation to create one. Vegoa community should develop a Social commons system where children or the elderly are taken care of by everyone. Every Vegoan including the elderly will continue to receive the basic income that is co-created by everyone to offer basic rights to every Vegoan.

I have an organisation and sell a product that needs outside raw materials and fee paid in euros, how can I be able to sell my product in vegetas?
----------------------------------------------------
* If your product’s fee and raw materials are entirely bought in euros therefore it is easy to believe that you have no other choice than selling your product entirely in euros.
* However as the community grows and more organisations offering products/services are created, you can tend to convert your product price entirely in vegetas.
* To encourage it, organisations can expose their expenses regularly to the community so other members/organisations know what is necessary to produce for the community to become self-sufficient.

Manifest
========

Every member that wants to join the Vegoa community has to comply with the basic rules of the Manifest.

1. All members joining Vegoa are committed to a cruelty-free vegan lifestyle.
2. Each member of the Vegoa community (hereinafter referred to as: Vegoan) gives their best effort to protect the environment and promote self-sustainability.
3. Each Vegoan respects the rights and freedom of its fellow Vegoans.
4. The rights and freedom of each Vegoan inside the community shall only be limited to the extent that they interfere inappropriately with the rights and freedoms of other Vegoans.
5. There shall be no discrimination based on gender, age, religion, race, sexual orientation or disabilities between Vegoans.
6. Vegoans agree to respect Portuguese law, in particular tax and criminal law.
7. A Vegoan living in a private house built in a co-housing land cannot be expropriated and can trade their house.
8. Vegoans can only be excluded from the community when they fail to respect the Manifest with a 90% vote.
9. Every Vegoan has the right to a Basic Income.
10. Each new member needs to go through a trial time of 6 months, after which they automatically become a Vegoan unless a third of the members disapprove in an official vote with the new member joining.
11. The Manifest can only be changed with a majority vote of 90%.

FAQ
===

How can I suggest an update to the Manifest?
--------------------------------------------
* You can improve the Manifest by suggesting a new version following the semantic MAJOR.MINOR.PATCH inspired by Semantic Versioning.
* However 90% of the present Vegoa members have to approve any new changes to endorse a new version of the Manifest and release it.
* Given a version number MAJOR.MINOR.PATCH, increment the:
  - MAJOR version when incompatible Manifest changes are made,
  - MINOR version when rules in a backwards-compatible manner are added, and
  - PATCH version when backwards-compatible consistency/typo fixes are done.
